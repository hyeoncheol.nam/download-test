const express = require('express');
const dotenv = require('dotenv');

dotenv.config();
const router = require('./router');

const app = express();
app.disable('x-powered-by');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/', router);
app.use((req, res) => res.status(404).send('404 Not Found'));
app.listen(3000, () => console.log('Listening on port 3000'));

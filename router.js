const mysql = require('mysql2');
const csv = require('fast-csv');
const redis = require('redis');
const { Transform } = require('stream');

const preset = {
  platform: ['Twitch', 'YouTube'],
  status: ['精算管理', '振込待ち', '振込完了', '振込保留']
};

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  timezone: process.env.DB_TIMEZONE
});

const transform = () => new Transform({
  readableObjectMode: true,
  writableObjectMode: true,
  transform(chunk, _, callback) {
    chunk['プラットホーム'] = preset.platform[chunk['プラットホーム'] - 1];
    chunk['精算状態'] = preset.status[chunk['精算状態'] - 1];
    this.push(chunk);
    callback();
  }
});

const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT
});

module.exports = require('express').Router()
  .get('/:id', (req, res) => { 
    client.hgetall(req.params.id, (err, data) => {
      if (err || !data) {
        return res.status(404).send('404 Not Found');
      }
      try {
        const { pk, start, end } = data;
        if (!pk) {
          return res.status(404).send('404 Not Found');
        }
        client.del(req.params.id);
        res.attachment(`Donation${start}-${end}.csv`);
        res.write('\ufeff');
        connection
          .query('SELECT DATE_FORMAT(d.created_at, "%Y-%m-%d %H:%i:%S") AS どね時刻, v.platform AS プラットホーム, v.id AS どねID, d.nickname AS どねニックネーム, d.amount AS どね金額, d.real_amount AS 実際精算金額, d.message AS メッセージ, d.status AS 精算状態 FROM donations AS d JOIN viewers AS v ON v.pk = d.viewer_pk WHERE d.streamer_pk = ? AND d.created_at BETWEEN ? AND ? ORDER BY d.created_at DESC',
            [pk, start, end])
          .stream()
          .pipe(transform())
          .pipe(csv.format({ headers: true }))
          .pipe(res);
      } catch (e) {
        console.error(e);
        res.status(404).send('404 Not Found');
      }
    })
  })
  .post('/', (req, res) => { 
    if (req.headers.developer !== process.env.SIGNATURE) {
      return res.status(404).send('404 Not Found');
    }
    try {
      client.hmset(req.body.id, 'pk', req.body.key, 'start', req.body.start, 'end', req.body.end);
      client.expire(req.body.id, 600);
      res.json({ ok: true });
    } catch (e) {
      console.error(e);
      res.status(404).send('404 Not Found');
    }
  });